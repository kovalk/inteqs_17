clear all;
close all;
clc; 

n = 500; % number of points
T = 2*pi; % end of interval 
h = T/n; 

% Build up matrix K
Kij = zeros(n,n);
for i = 1:n
    ti = i*h;
    for j = 1:n
        tj = j*h; 
        Kij(i,j) = K(ti,tj);
    end
end

% Build up rhs, f
f = zeros(n,1); 
for i = 1:n
    f(i,1) = ft(h*i); 
end

star = 3; 
tstar = h*star; 
uniqConst = zeros(n,n); 
uniqConst(:,star) = 1; 

A = 1/2*eye(n,n)+uniqConst+(h*Kij);
rho = A\f; 

%conv_test_circ()
%conv_test([0,0]',[1/2,1/2]')
ry = linspace(0,1);
t = linspace(0,2*pi);
d = length(ry);
XX = zeros((d-2),d);
YY = zeros((d-2),d);
Z = zeros((d-2),d); 
Ztrue = zeros((d-2),d); 

for i = 2:d-1
    for j = 1:d
    end
end


for i = 2:d-1
    for j = 1:d
        x = [ry(i)*2*cos(t(j)),ry(i)*sin(t(j))]; 
        Spx = S_pot(x(1,1),x(1,2)); % exact soln at x
        apprx = 0; 
        for k = 1:n
          Kxj =  S_pot_s(x(1,1),x(1,2),k*h); 
          apprx = apprx+(h*Kxj*rho(k,1));  
        end
        Z(i-1,j) = apprx;
        Ztrue(i-1,j) = Spx; 
        XX(i-1,j) = x(1,1); 
        YY(i-1,j) = x(1,2); 
    end
end

Z = Z-(Z(1,1)-Ztrue(1,2));
figure;
subplot(1,2,1)
contourf(XX,YY,Z,400,'edgecolor','none')
title('Approximated solution')
xlabel('x')
ylabel('y')
subplot(1,2,2)
contourf(XX,YY,Ztrue,400,'edgecolor','none')
title('Approximated solution')
xlabel('x')
ylabel('y')


%% 

% Parametrization of curve
function rt = r_t(t)

%rt = [cos(t),sin(t)];
rt = [2*cos(t),sin(t)];

end

% tangent to curve
function drt = r_tprime(t)

%drt = [-1*sin(t),cos(t)];
drt = [-2*sin(t),cos(t)];

end

% second derivative of parametrization
function d2rt = d2r_t(t)

%d2rt = [-cos(t),-sin(t)];
d2rt = [-2*cos(t),-1*sin(t)];

end

% Normal to the boundary, pointing in
function nt = n_t(t)

%nt = -1*[cos(t),sin(t)];
nt = -1*[cos(t),2*sin(t)];

for i = 1:length(t)
    nt(i,:) = nt(i,:)/norm(nt(i,:)); 
end

end 

% neumann data, f, for tests use d/dn(g(x,y)), assuming (2,0) is not on the
% boundary 
function f_t = ft(t)

xstar = [3.,0];

nt = n_t(t); % normal to Gamma at t
rt = r_t(t); 

diff = rt-xstar; % r(t)-r(s)
diffn = diff*nt'; % [r(t)-r(s)]*n(t)
f_t = (1/(2*pi))*diffn/(norm(diff)^2); 
%f_t = cos(t);

end

% kernel, K(t,s), t, s scalars 
function Kts = K(t,s)

nt = n_t(t); % normal to Gamma at t
rsprime = r_tprime(s); % r'(s)
rtprime = r_tprime(t); % r'(t)
d2rt = d2r_t(t); % r''(t)
rt = r_t(t);
rs = r_t(s);

diff = rt-rs; % r(t)-r(s)
diffn = diff*nt'; % [r(t)-r(s)]*n(t)

if t == s
    Kts = (1/(4*pi))*(d2rt(1,1)*rtprime(1,2)-d2rt(1,2)*rtprime(1,1))/(norm(rtprime)^2); 
end

if t ~= s 
    Kts = (1/(2*pi))*diffn*norm(rsprime)/(norm(diff)^2); 
end

end 

function Sp = S_pot(x,y)

xstar = [3.,0];
distx = (x-xstar(1,1))^2; 
disty = (y-xstar(1,2))^2; 
dist = sqrt(distx+disty);
Sp = (1/(2*pi))*(log(dist));

end

function Sp = S_pot_s(x,y,s)

rsprime = r_tprime(s); 
rs = r_t(s);
distx = (x-rs(1,1))^2; 
disty = (y-rs(1,2))^2; 
dist = sqrt(distx+disty);
Sp = (1/(2*pi))*(log(dist))*norm(rsprime);

end

function conv_test_circ()

for i = 1:10
    n = 2^i; % number of points
    T = 2*pi; % end of interval 
    h = T/n; 
    
    Kij = zeros(n,n);
    for j = 1:n
       tj = j*h;
       for k = 1:n
          tk = k*h; 
          Kij(j,k) = K(tj,tk);
       end
   end
   % Build up rhs, f
   f = zeros(n,1); 
   for j = 1:n
      f(j,1) = ft(h*j); 
   end
   
   star = 2; 
   tstar = h*star; 
   uniqConst = zeros(n,n); 
   uniqConst(:,star) = 1; 
   
   A = 1/2*eye(n,n)+uniqConst+(h*Kij);
   rho = A\f; 
   format long; 
   sprintf('%d   %e',i,abs(2*(f(n/2,1)-f(n,1))-(rho(n/2,1)-rho(n,1))))
   
    
end

end

function conv_test(x,y)

Sp1 = S_pot(x(1,1),x(2,1)); % exact solns at x and y
Sp2 = S_pot(y(1,1),y(2,1));

for i = 1:10
    n = 2^i; % number of points
    T = 2*pi; % end of interval 
    h = T/n; 
    
    Kij = zeros(n,n);
    for j = 1:n
       tj = j*h;
       for k = 1:n
          tk = k*h; 
          Kij(j,k) = K(tj,tk);
       end
   end
   % Build up rhs, f
   f = zeros(n,1); 
   for j = 1:n
      f(j,1) = ft(h*j); 
   end

   star = 2; 
   uniqConst = zeros(n,n); 
   uniqConst(:,star) = 1; 
   
   A = 1/2*eye(n,n)+uniqConst+(h*Kij);
   rho = A\f;
   
   apprx = 0; 
   appry = 0;
   
   for j = 1:n
       Kxj =  S_pot_s(x(1,1),x(2,1),j*h);
       Kyj = S_pot_s(y(1,1),y(2,1),j*h); 
       apprx = apprx+(h*Kxj*rho(j,1)); 
       appry = appry+(h*Kyj*rho(j,1));       
   end
   
   format long; 
   sprintf('%d   %e',i,abs(Sp1-Sp2-(apprx-appry)))
   
    
end

end