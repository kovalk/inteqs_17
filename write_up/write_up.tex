\documentclass[a4paper,11pt]{article}

\usepackage{geometry}
\usepackage{listings}
\usepackage{mathrsfs}
\usepackage[font={small},labelfont={sf,bf}]{caption}
\usepackage{color}
\usepackage{amssymb}
\usepackage[utf8]{inputenc}
\usepackage{afterpage}
\usepackage[T1]{fontenc}
\usepackage{amsfonts}
\usepackage{mathtools}
\usepackage{amsmath}
\usepackage{verbatim}
\usepackage{bm}
\usepackage{url}
\usepackage{bbm}
\usepackage{enumerate}
\usepackage{amsthm}
\usepackage{stmaryrd}
\usepackage{tabu}

\geometry{a4paper,top=3cm,bottom=3cm,left=2cm,right=2cm,heightrounded, bindingoffset=5mm}

\newcommand{\n}{\mathbf{n}}
\newcommand{\x}{\mathbf{x}}
\newcommand{\y}{\mathbf{y}}
\newcommand{\br}{\mathbf{r}}


\begin{document}
\date{}
\title{Integral Equations and Fast Algorithms Course Project}
\author{Karina Koval}
\maketitle
\section*{Problem Introduction}
We consider the interior Laplace equation with Neumann boundary data. Given a smooth, simply connected domain $\Omega$, and a function, $f \in L^2(\Omega)$, we look for a function $u: \bar{\Omega} \rightarrow \mathbb{R}$ such that: 
\begin{equation}
\left\{
	\begin{array}{ll}
		\Delta{u}(\x) = 0  \hspace{5mm} \mbox{in} \hspace{3mm} \Omega \\
		\nabla{u}(\x) \cdot \n(\x) = f(\x) \hspace{5mm} \mbox{on} \hspace{3mm} \partial\Omega \
	\end{array}
\right.
\end{equation} 
We assume that $\Omega \subset \mathbb{R}^2$, and can be parametrized by $\br(t) = (\xi (t),\eta (t))$ for $t \in [0,2\pi]$. \\
In section 1 we show that all solutions to $(1)$ are unique up to an additive constant. In section 2 we write the solution $u(\x)$ as a single layer potential due to an unknown distribution, $\sigma(\x)$ and develop an integral equation which $\sigma$ must satisfy. Section 3 outlines the numerical scheme used to solve the integral equation and section 4 shows the application of the scheme to solve equation $(1)$ for a particular choice of domain $\Omega$ and data $f(\x)$. 
\paragraph*{}

\section*{1. Existence and Uniqueness}\label{1}
First we integrate $(1)$ and apply the divergence theorem to obtain a necessary condition for the well-posedness of the Neumann problem. 
$$
0 = \int_{\Omega} \nabla \cdot (\nabla{u}) dA = \int_{\partial\Omega} \nabla{u}\cdot \n{(\x) } ds(\x) = \int_{\partial\Omega} f(\x) ds(\x) 
\implies \int_{\partial\Omega} f(\x) ds(\x) = 0 
$$
We use Green's first identity $(2)$ to show uniqueness up to an additive constant. 
\begin{equation}
\int_{\Omega} (\nabla{u}\cdot \nabla{v} + u\Delta{v}) dV = \int_{\partial\Omega} u \nabla{v}\cdot \n dA 
\end{equation}
\\
\\
\textbf{\textit{Theorem}} Assuming a solution $u(\x)$ of $(1)$ exists, it is unique up to an arbitrary additive constant. 
\\
\textbf{Proof:} Let $u_1$ and $u_2$ be two solutions to the interior Neumann Laplace problem. Then $v = u_1-u_2$ satisfies the following: 
$$
\left\{
	\begin{array}{ll}
		\Delta{v}(\x) = 0  \hspace{5mm} \mbox{in} \hspace{3mm} \Omega \\
		\nabla{v}(\x) \cdot \n(\x) = 0 \hspace{5mm} \mbox{on} \hspace{3mm} \partial\Omega \
	\end{array}
\right.
$$
Letting $u = v$ in Green's identity $(2)$, we are left with:
$$ 
\int_{\Omega} |\nabla{v}|^2 dV = 0 \implies \nabla{v} = 0 \implies v\mbox{ is constant.}
$$
Since we do not have dirichlet data, we can not determine the constant. Thus, if we have two different solutions, $u_1$ and $u_2$, $u_1-u_2 = c$ for arbitrary constant, $c$. 
\\
\\
We use a variational approach to show that a solution to $(1)$ exists. 

To begin with, we rewrite $(1)$ in weak form by multiplying by a test function $v$ and applying Green's theorem. This leads to: 
\begin{equation}
\int_{\Omega}\nabla u \cdot \nabla v dA = \int_{\partial\Omega} f v ds
\end{equation}
We now introduce, $V$, the set of all $H^1(\Omega)$ functions that are orthogonal to the constants in the $L^2(\Omega)$ inner product.  
$$
V = \{ v \in H^1(\Omega) : \int_{\Omega} v dA = 0 \}
$$
\\
\\
\textbf{\textit{Claim:}} Let $a(u,v) = \int_{\Omega} \nabla u \cdot \nabla v dA$, and $l(v) = \int_{\partial\Omega} vf ds$. Then $\exists u \in V$ such that: $a(u,v) = l(v)$. 
\\
\textbf{Proof:} The proof uses the Lax-Milgram theorem as well as the Poincar\'e-Wirtinger inequality. 
\\
\\
The Poincar\'e-Wirtinger inequality states that: if $\Omega \subset \mathbb{R}^2$ is Lipschitz, then $\exists$ a constant, $ C(\Omega)$ such that $\forall v \in H^1(\Omega)$,

$$
\| v - \frac{1}{\mu(\Omega)}<v,1>_{L^2(\Omega)} \|_{L^2(\Omega)} \leq C(\Omega)\|\nabla v\|_{L^2(\Omega)}
$$
\\ 
Here, $\mu(\Omega)$ denotes the measure of $\Omega$. 

To apply Lax-Milgram, we must show that $V$ is a Hilbert space, $a(u,v)$ is coercive with respect to $V$, and $a$ and $l$ are both continuous. 
\\
Let $\{ v_n \}$ be a sequence in $V$ such that $v_n \rightarrow v \in H^1(\Omega)$. Since $\Omega$ is bounded, we have that $H^1(\Omega) \subset L^2(\Omega)$ which implies that $v_n \rightarrow v \in L^2(\Omega)$. Then, by Cauchy-Schwarz, we have that $\|v_n\|_{L^1(\Omega)} \leq C\|v_n\|_{L^2(\Omega)}$ which implies that $v_n \rightarrow v \in L^1(\Omega)$. Thus we have that $\int_{\Omega} v_n dA \rightarrow \int_{\Omega} v dA = 0$ which implies that $v_n \rightarrow v \in V$ so $V$ is closed and thus a Hilbert space. 
\\
\\
Now we will show that $a(v,v) = \|\nabla v \|_{L^2(\Omega)} \geq C \|v\|_H^1{(\Omega)}$, i.e., that $a(u,v)$ is coercive. 
\\
$\forall v \in V$, we have that (by the Poincar\'e-Wirtinger inequality):
$$
\|v\|_{H^1(\Omega)} = \| v - \frac{1}{\mu(\Omega)}<v,1>_{L^2(\Omega)} \|_{L^2(\Omega)} + \|\nabla v\|_{L^2(\Omega)} \leq K\|\nabla v\|_{L^2(\Omega)}
$$  
The continuity of $a(u,v)$ and $l(v)$ follow from an application of Cauchy-Schwartz. 
$$
|a(u,v)| = | \int_{\Omega} \nabla u \cdot \nabla v dA | \leq \int_{\Omega} |\nabla u \cdot \nabla v | dA \leq \|\nabla u\|_{L^2(\Omega)}\|\nabla v\|_{L^2(\Omega)} \hspace{3mm} \mbox{(by C.S)} \leq \| u\|_{H^1(\Omega)} \|v\|_{H^1(\Omega)}
$$
The proof that $l(v)$ is continuous follows the same steps. 

\paragraph*{}

\section*{2. Integral Equation and Invertibility}
\paragraph*{}
We begin by representing the solution $u(\x)$ in $\Omega$ in terms of a single layer potential due to an unknown distribution on the boundary, $\sigma$. 
\begin{equation}
u(\x) = \mathcal{S}\sigma(\x) = \frac{1}{2\pi}\int_{\partial\Omega} \log|\x-\y| \sigma(\y) ds(\y)
\end{equation}
To obtain an integral equation for $\sigma$, we ensure the Neumann boundary condition is satisfied, i.e., letting $\x^* = \x+\delta\n(\x)$, where $\x \in \partial\Omega$ 
$$ 
\lim_{\delta \rightarrow 0^-} \nabla u(\x^*) \cdot \n(\x) = \lim_{\delta \rightarrow 0^-} \frac{1}{2\pi} \int_{\partial\Omega} \frac{\x^*-\y}{|\x^*-\y|^2}\cdot \n(\x) \sigma(\y) ds(\y) = \lim_{\delta \rightarrow 0^-} \mathcal{S}'\sigma{(\x^*)} = f(\x)
$$
We saw in class that $\lim_{\delta \rightarrow 0^-} \mathcal{S}'\sigma{(\x^*)} = \frac{1}{2}\sigma{(\x)} + \mathcal{S}'\sigma{(\x)}$, thus we obtain the following integral equation for $\sigma$: 
\begin{equation}
\frac{1}{2}\sigma{(\x)}+\mathcal{S}'\sigma{(\x)} = f(\x)
\end{equation} 
Here, $\mathcal{S}'\sigma{(\x)} = \frac{1}{2\pi}\int_{\partial\Omega} \frac{\x-\y}{|\x-\y|^2}\cdot \n(\x) \sigma{(\y)} ds(\y)$

Using our parametrization for $\partial\Omega$, we can rewrite $(3)$ as follows: 
\begin{equation}
\frac{1}{2}\sigma (t) + \int_0^{2\pi} K(t,s) \sigma (s) ds = f(t)
\end{equation}
Where $f(t) = f(\x(t))$, $\sigma (t) = \sigma (\x(t))$, and 
\begin{equation}
K(t,s) = \left\{
	\begin{array}{ll}
		\frac{1}{2\pi} \frac{\xi '(t)(\eta (t)-\eta (s)) - \eta '(t)(\xi (t)-\xi (s))}{(\xi (t) - \xi (s))^2+(\eta (t) - \eta (s))^2} \sqrt{\frac{\xi '^2(s) + \eta '^2 (s)}{\xi '^2(t) + \eta '^2 (t)}} \hspace{5mm} t \not = s \\
		\frac{1}{4\pi} \frac{\eta '(t)\xi ''(t)- \xi '(t)\eta ''(t)}{\xi '(t)^2 + \eta '(t)^2} \hspace{5mm} t = s \
	\end{array}
\right.
\end{equation}
Here, $K(t,t)$ was obtained by taking $\lim_{s \rightarrow t} K(t,s)$. This was done by applying L'Hospital two times. 

Denote $(5)$ abstractly as 
\begin{equation}
(\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma = f
\end{equation} 
\\
\\
\textbf{\textit{Claim}:} $\frac{1}{2}\mathcal{I}+\mathcal{K}$ has a non-trivial null-space of dimension 1. 
\\
\textbf{Proof:} 
Note that the adjoint of $\mathcal{K}: C(\Omega) \rightarrow C(\Omega)$ with respect to the $L^2(\partial\Omega)$ inner product is defined such that: $\mathcal{K}^*\sigma(\x) = \frac{1}{2\pi} \int_{\partial\Omega} \nabla \log|\x-\y| \cdot \n(\y) \sigma(\y) ds(\y) = \mathcal{D}\sigma(\x)$. This implies that $ (\frac{1}{2}\mathcal{I}+\mathcal{K})^* = \frac{1}{2}\mathcal{I}+\mathcal{K}^*$ 

Let $\sigma(\x)$ be such that $(\frac{1}{2}\mathcal{I}+\mathcal{K}^*)\sigma(\x)=(\frac{1}{2}\mathcal{I}+\mathcal{D})\sigma(\x) = 0$. 

Define $u^{out}(\x) = \mathcal{D}\sigma(\x)$ for $\x \in \mathbb{R}^2 \setminus \Omega$. By the jump properties for the double-layer potential we saw in class, $\lim_{h \rightarrow 0^+} \mathcal{D}\sigma(\x+h\n(\x)) = (\frac{1}{2}I+\mathcal{D})\sigma(\x) = 0$. This implies that $u^{out}$ solves the exterior Dirichlet problem with zero boundary condition. Since $u^{out}(\x) = \mathcal{D}\sigma (\x) \sim O(\frac{1}{|\x|})$ as $|\x| \rightarrow \infty$, $u^{out}(\x) = 0 $ for $x \in \mathbb{R}^2 \setminus \Omega$

Now let $u^{in}(\x) = \mathcal{D}\sigma(\x)$ for $\x \in \Omega$. Since $\lim_{h \rightarrow 0^{\pm}} \mathcal{D}'\sigma(\x+h\n(\x)) = \mathcal{D}'\sigma(\x)$, we have that: $\nabla u^{in}(\x) \cdot \n(\x) = \nabla u^{out}(\x) \cdot \n(\x) = 0$. Thus, $u^{in}$ is a solution to the interior Neumann problem with 0 boundary data. As we saw in section 1, this means that $u^{in}$ is a constant.  

By using the jump condition for $\mathcal{D}\sigma{(\x)}$, we can see that $\sigma = \lim_{h \rightarrow 0} (u^{out}(\x+h\n)-u^{in}(\x-h\n)) = c$, so $\sigma = c$ is a non-trivial solution to $(\frac{1}{2}\mathcal{I}+\mathcal{K}^*)\sigma = 0$

To finish the argument, we use the Fredholm Alternative. $1 = \dim \mathcal{N}(\frac{1}{2}\mathcal{I}+\mathcal{K}^*) = \dim \mathcal{N}(\frac{1}{2}\mathcal{I}+\mathcal{K})$.
\\
\\
Note that this implies that $(5)$ is solveable so long as $f$ is orthogonal (with respect to the $L^2$ inner product) to constant functions, i.e., $\int_{\partial\Omega} f ds = 0$.
To make the integral equation uniquely solveable, we follow the idea developed in [1]. 

Rather than solving $(8)$, we introduce a modified integral equation $(8)$. Let $t^* \in [0,2\pi]$.
\begin{equation}
(\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma + \sigma(t^*) = f
\end{equation}
This equation is uniquely solveable and produces one of the solutions to $(8)$. The full proof is in [1], but the main ideas of the proof are: 

To show solveability, note that $(\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma + \sigma(t^*) = 0 \implies (\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma = -\sigma(t^*)$. By the Fredholm Alternative, this integral equation has a solution iff $\sigma(t^*) = 0$. Let $\Psi$ be an eigenfunction of $(8)$. Then $\sigma = \lambda \Psi$. Since $0 = \sigma(t^*) = \lambda \Psi(t^*)$, for our particular choice of $\partial\Omega$, we have that $\lambda = 0$, thus $\sigma = 0$ is the only solution.  

To show that $\sigma$ satisfies $(8)$, we rewrite $(9)$ as: $(\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma-f = \sigma(t^*)$. Since $f$ is orthogonal to the constant functions, we have that $(\frac{1}{2}\mathcal{I}+\mathcal{K})\sigma-f \in$ Range$(\frac{1}{2}\mathcal{I}+\mathcal{K})$, which again implies that $\sigma(t^*) = 0$. 

\section*{3. Numerical Implementation}
Since the kernel $K(t,s)$ is smooth and $2\pi$ periodic, we can use trapezoidal rule since it is exponentially convergent for $2\pi$ periodic functions. 

The discrete form of $(9)$ is thus: 
\begin{equation}
\frac{1}{2}\sigma(t_i) + h \sum_{j=1}^N K(t_i,t_j) \sigma(t_j) + \sigma(t^*)= f(t_i)
\end{equation}
where $h = \frac{2\pi}{N}$, $t_i = hi$ for all $i \in [1,2,...,N]$, and $t^* = kh$ for some $k \in [1,2,...,N]$. 

The resulting linear system we need to solve is: 
\begin{equation}
(\frac{1}{2}\mathbf{I}+\mathbf{K}+\mathbf{A})\sigma = \mathbf{f}
\end{equation}
here, $I$ is the $N \times N$ identity matrix, $K \in \mathbb{R}^{N \times N}$ with $K_{ij} = K(t_i,t_j)$, $A \in \mathbb{R}^{N \times N}$ with 1s in the $k$th column and 0s elsewhere, and $f \in \mathbb{R}^{N \times 1}$ with $f_i = f(t_i)$. 

Once $\sigma$ is known, we can plug it into the single-layer potential and again use the $2\pi$ periodic trapezoidal rule to discretize (this should be accurate far away enough from the boundary). This leads to: 
\begin{equation}
\hat{u}(\x) = h\sum_{i=1}^N \log |x-\br(t_i)| \sigma(\br(t_i))  
\end{equation}

The scheme was tested on the following problem: 
\begin{equation}
\left\{
	\begin{array}{ll}
		\Delta{u}(\x) = 0  \hspace{5mm} \mbox{in} \hspace{3mm} \Omega \\
		\nabla{u}(\x) \cdot \n(\x) = \frac{1}{2\pi} \nabla \log|\x - \x^*| \cdot \n(\x) \hspace{5mm} \mbox{on} \hspace{3mm} \partial\Omega \
	\end{array}
\right.
\end{equation}
for $x^*$ outside $\Omega$. 

Since $\frac{1}{2\pi}\log |\x-\x^*|$ is harmonic $\forall \x \in \Omega$, $u(\x) = \frac{1}{2\pi} \log|\x-\x^*|$ is a solution to $(9)$.  

Because $\hat{u} = \bar{u}+c$ for some arbitrary unknown constant, using $| u(\x) - \hat{u}(\x)|$ to compute the error would be incorrect. Instead, we compute: 
\begin{equation}
E(x,y) = | u(\x)-u(\y) - (\hat{u}(\x)-\hat{u}(y))|
\end{equation}
\paragraph*{}

\section*{4. Plots and Error Results}
For the following, $\Omega$ was chosen to be an ellipse with the parametrization: $\br(t) = (2\cos(t),sin(t))$. 
\vspace{5mm}
\begin{figure}[h!]
  \caption{Comparison of true and approximated solution using 400 quadrature nodes}
  \centering
    \includegraphics[clip, trim=0.5cm 11cm 0.5cm 11cm, scale=0.8]{ellipsecomparison}
\end{figure}

For the convergence results, the mesh size was taken to be $2^i$, $i \in [1,2,..10]$. The error was calculated as described in section 3 with $x = (0,0)$ and $y = (0.5,0.5)$. The convergence rate doesn't seem to be exactly exponential, which may be due to cancellation error in computation of the kernel. Regardless, we can see that only $2^7$ nodes are needed to compute the solution to machine precision. 

\begin{center}
    \begin{tabular}{| l | l |}
    \hline
    $i$ & Error  \\ \hline
    1 & $1.5969 \times 10^{-1}$ \\
    2 &  $8.8699 \times 10^{-3} $\\
    3 &  $5.3366 \times 10^{-4} $\\
    4 &  $7.6625 \times 10^{-5} $\\
    5 &  $ 2.2822 \times 10^{-6} $\\
    6 &  $ 3.0497 \times 10^{-10} $\\
    7 &  $5.5511 \times 10^{-17} $\\
    8 &  $8.3267 \times 10^{-17}$ \\
    9 &  $8.3267 \times 10^{-17}$ \\
    10 &  $ 2.4980 \times 10^{-16}$ \\
 
    \hline
    \end{tabular}
\end{center}


\paragraph*{}



\begin{thebibliography}{9}
\bibitem{atkinson:numericsolutions,} 
Kendall E. Atkinson
\textit{The Numerical Solution of Integral Equations of the Second Kind}. 
Cambridge university press, 1997
 
\bibitem{barnett:BVPs,} 
Alex Barnett 
\textit{Boundary integral equations for BVPs, and their high-order Nystrom quadratures: a tutorial}. 
\\\texttt{https://math.dartmouth.edu/~fastdirect/notes/quadr.pdf}
\end{thebibliography}



\end{document}

